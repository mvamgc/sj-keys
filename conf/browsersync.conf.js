const conf = require('./gulp.conf');
const proxy = require('http-proxy-middleware');

const echoProxy = proxy('/echo', {
  target: 'http://localhost:9999',
  changeOrigin: true,
  ws: true,
  logLevel: 'debug'
});


module.exports = function () {
  return {
    server: {
      baseDir: [
        conf.paths.tmp,
        conf.paths.src
      ],
      middleware: [echoProxy]
    },
    open: false
  };
};
