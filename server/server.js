const express = require('express');
const sockjs = require('sockjs');
const http = require('http');

// 1. Echo sockjs server
const sockjsOpts = {sockjs_url: 'http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js'};

const channelIndex = {};
const connectionIndex = {};

const sockjsEcho = sockjs.createServer(sockjsOpts);
sockjsEcho.on('connection', conn => {
  console.log('start');
  console.log(conn.id);
  connectionIndex[conn.id] = {con: conn};
  console.log('connections: ' + JSON.stringify(Object.keys(connectionIndex)));
  console.log('channels: ' + JSON.stringify(channelIndex));

  conn.on('data', message => {
    // conn.write(message);
    const keysData = JSON.parse(message);
    console.log(keysData);

    if (!channelIndex[keysData.channel]) {
      channelIndex[keysData.channel] = {};
    }
    channelIndex[keysData.channel][conn.id] = true;

    // console.log(conn.id);
    // channelIndex[keysData.channel] = channelIndex[keysData.channel].filter(conn => channelIndex.hasOwnProperty(keysData.channel));
    // channelIndex[keysData.channel]
    for (const conInChannel in channelIndex[keysData.channel]) {
      if (connectionIndex[conInChannel]) {
        console.log(conInChannel);
        connectionIndex[conInChannel].con.write(message);
      } else {
        delete channelIndex[keysData.channel][conInChannel];
      }
    }
    console.log('data');
    console.log('connections: ' + JSON.stringify(Object.keys(connectionIndex)));
    console.log('channels: ' + JSON.stringify(channelIndex));
  });
  conn.on('close', () => {
    if (connectionIndex[conn.id]) {
      delete connectionIndex[conn.id];
    }
    console.log('close');
    console.log('connections: ' + JSON.stringify(Object.keys(connectionIndex)));
    console.log('channels: ' + JSON.stringify(channelIndex));
  });
});

// 2. Express server
const app = express(); /* express.createServer will not work here */
const server = http.createServer(app);

sockjsEcho.installHandlers(server, {prefix: '/echo'});

console.log(' [*] Listening on 0.0.0.0:9999');
server.listen(9999, '0.0.0.0');

app.use(express.static('dist'));

// app.get('/', function (req, res) {
//     res.sendfile(__dirname + '../index.html');
// });
