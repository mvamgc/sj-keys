import * as $ from 'jquery';
import * as angular from 'angular';

import {main} from './app/main';
import {midiMain} from './app/midi-main';
import {dataConnector} from './app/data-connector';
import {MidiFactory} from './app/midi.service';

import 'angular-ui-router';
import routesConfig from './routes';

import './index.less';

export const app: string = 'app';

// console.log(MidiFactory);

angular
  .module(app, ['ui.router'])
  .config(routesConfig)
  .factory('dataConnector', dataConnector)
  .service('midiFactory', MidiFactory)
  .component('app', midiMain);
  // .component('app', main);

import './app/web-audio.service';
