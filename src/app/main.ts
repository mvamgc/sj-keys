
class MainCtrl {
  maxLatencyArrayLength = 20;

  channel: string;
  keysState;
  keyEvent;
  sendingKeys;
  receivingKeys;
  topLatencyMessages: any[];
  dataConnector;

  /** @ngInject */
  constructor($scope: angular.IScope, dataConnector, WebAudioFactory) {
    console.log('MainCtrl constructor');
    this.channel = '123';
    this.keysState = {};
    this.sendingKeys = {};
    this.receivingKeys = {};

    this.topLatencyMessages = [];

    this.dataConnector = dataConnector;

    // midiFactory.start2();
    // WebAudioFactory.start();

    dataConnector.onReceive(keys => {
      $scope.$apply(() => this.receivingKeys = keys);
      console.log('received in controller: ' + this.receivingKeys);
      this.topLatencyMessages.push(keys);
      this.topLatencyMessages = this.topLatencyMessages.sort((a, b) => b.latency - a.latency);
      if (this.topLatencyMessages.length > this.maxLatencyArrayLength) {
        this.topLatencyMessages.length = this.maxLatencyArrayLength;
      }
    });
  }

  onKeyEvent(event: KeyboardEvent) {
    if (event.type === 'keydown' || event.type === 'keyup') {
      // console.log('Key event1: %s, %s', event.type, event.key);
      var send = false;
      if (event.type === 'keydown') {
        send = !this.keysState[event.key];
        this.keysState[event.key] = true;
        this.keyEvent = {type: event.type, key: event.key};
      }
      if (event.type === 'keyup') {
        send = true;
        delete this.keysState[event.key];
        this.keyEvent = {type: event.type, key: event.key};
      }
      if (send) {
        const time = new Date();
        this.sendingKeys = {time: time, channel: this.channel, keys: this.keysState, keyEvent: this.keyEvent};
        this.dataConnector.sendKeys(this.sendingKeys);
      }
    }
  };
}

export const main: angular.IComponentOptions = {
  template: require('./main.html'),
  controller: MainCtrl
};
