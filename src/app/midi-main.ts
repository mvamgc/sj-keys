import * as WebMidi from 'webmidi';

class MidiMainCtrl {
    outputs: any[] = [{id:'test'}];;
    
    /* @ngInject */
    constructor($scope) {
        this.start($scope);
    }
    
    start($scope) {
        console.log('webmidi: ' + WebMidi);
        WebMidi.enable(err => {
            if (err) {
                console.log('WebMidi could not be enabled.', err);
            } else {
                console.log('WebMidi enabled!');
                console.log(WebMidi.outputs);
                $scope.$apply(() => {
                    this.outputs = WebMidi.outputs;
                    // this.outputs = [{id:'test2'}];
                });
                
                var output = WebMidi.outputs[1];
                console.log('playing[1]');
                // output.playNote('C3');
                // output.playNote('Gb4', 3);
                // output.playNote(['C3', 'D#3', 'G3'], 7);
                // output.playNote('F#-1', 'all', {velocity: 1});
                // output.playNote('F5', 16, {time: '+2000'});
                // output.playNote('F5', 16, {time: WebMidi.time + 3000});
                // output.playNote('Gb2', 10, {duration: 2000, velocity: 0.25});
                // output.stopNote('C-1');
        // output.playNote('G5', 12)
        //   .sendPitchBend(-0.5, 12, {time: 400}) // After 400 ms.
        //   .sendPitchBend(0.5, 12, {time: 800})  // After 800 ms.
        //   .stopNote('G5', 12, {time: 1200});    // After 1.2 s.

                let filterNoteData = note => {
                    return {
                        data: note.data,
                        note: note.note,
                        rawVelocity: note.rawVelocity,
                        timestamp: note.timestamp,
                        type: note.type,
                        channel: note.channel,
                        velocity: note.velocity,
                        receivedTime: note.receivedTime,
                        value: note.value,
                        controller: note.controller
                    };
                };
                var input = WebMidi.inputs[1];
                var allNotes = [];
                
                input.addListener('noteon', 'all', function(e) {
                    let noteData = filterNoteData(e);
                    console.log("noteon: %o", JSON.stringify(noteData));
                    allNotes.push(noteData);
                    console.log("noteon: %o", e);
                    // console.log("all: %o", JSON.stringify(allNotes));
                    
                    setTimeout(() => {
                        output.playNote(noteData.note.name + noteData.note.octave);
                    }, 2000);
                });
                input.addListener('noteoff', 'all', function(e) {
                    let noteData = filterNoteData(e);
                    console.log("noteoff: %o", JSON.stringify(noteData));
                    // console.log("noteoff: %o", noteData);
                    allNotes.push(noteData);
                    // console.log("all: %o", JSON.stringify(allNotes));
                });
                input.addListener('keyaftertouch', 'all', function(e) {
                    let noteData = filterNoteData(e);
                    console.log("keyaftertouch: %o", JSON.stringify(noteData));
                    allNotes.push(noteData);
                    // console.log("noteon: %o", noteData);
                    // console.log("all: %o", JSON.stringify(allNotes));
                });
                input.addListener('controlchange', 'all', function(e) {
                    let noteData = filterNoteData(e);
                    console.log('controlchange: %o', JSON.stringify(noteData));
                    allNotes.push(noteData);
                    console.log("controlchange: %o", noteData);
                    // console.log("all: %o", JSON.stringify(allNotes));
                });
                input.addListener('channelmode', 'all', function(e) {
                    let noteData = filterNoteData(e);
                    console.log('channelmode: %o', JSON.stringify(noteData));
                    allNotes.push(noteData);
                    // console.log("noteon: %o", noteData);
                    // console.log("all: %o", JSON.stringify(allNotes));
                });
                input.addListener('programchange', 'all', function(e) {
                    let noteData = filterNoteData(e);
                    console.log('programchange: %o', JSON.stringify(noteData));
                    allNotes.push(noteData);
                    console.log("programchange: %o", e);
                    // console.log("all: %o", JSON.stringify(allNotes));
                });
                input.addListener('channelaftertouch', 'all', function(e) {
                    let noteData = filterNoteData(e);
                    console.log('channelaftertouch: %o', JSON.stringify(noteData));
                    allNotes.push(noteData);
                    // console.log("noteon: %o", noteData);
                    // console.log("all: %o", JSON.stringify(allNotes));
                });
                input.addListener('pitchbend', 'all', function(e) {
                    let noteData = filterNoteData(e);
                    console.log('pitchbend: %o', JSON.stringify(noteData));
                    allNotes.push(noteData);
                    // console.log("noteon: %o", noteData);
                    // console.log("all: %o", JSON.stringify(allNotes));
                });
            }
        });
    }
}

export const midiMain: angular.IComponentOptions = {
  template: require('./midi-main.html'),
  controller: MidiMainCtrl
};
