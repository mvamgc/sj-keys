import * as SockJS from 'sockjs-client';

var sock = new SockJS('/echo', {}, {});

let receiveListeners = [];

sock.onopen = function() {
     console.log('open');
      // sock.send('test-1');
};

sock.onmessage = function(event) {
  console.log('message received at %s: ', JSON.stringify(new Date()), event.data);
  var data = JSON.parse(event.data);
  data.receivedTime = new Date();
  data.time = new Date(data.time);
  data.latency = data.receivedTime.getTime() - data.time.getTime();
  receiveListeners.forEach(f => f(data));
};

sock.onclose = function() {
     console.log('close');
};

//  sock.close();

function sendKeys(keysData: IKeysDataFrame) {
  // console.log('sending keys %s to %s', JSON.stringify(keysData.keys), keysData.channel);
  var data = JSON.stringify(keysData);
  sock.send(data);
}

function onReceive(f) {
  receiveListeners.push(f);
}

export interface IKeysDataFrame {
  time: Date;
  receivedTime?: Date;
  channel: string;
  keys: any;
}

export function dataConnector() {
  return {
    sendKeys: sendKeys,
    onReceive: onReceive
  };
};

