import * as WebMidi from 'webmidi';

export class MidiFactory {
  constructor() {
    console.log('constr');
  }
  start2() {
    console.log('webmidi: ' + WebMidi);
    WebMidi.enable(function (err) {
      if (err) {
        console.log('WebMidi could not be enabled.', err);
      } else {
        console.log('WebMidi enabled!');
        console.log(WebMidi.outputs);
        var output = WebMidi.outputs[0];
        console.log('playing[1]');
        output.playNote('C3');

        // Play a note on channel 3
        output.playNote('Gb4', 3);

        // Play a chord on all available channels
        output.playNote(['C3', 'D#3', 'G3']);

        // Play a chord on channel 7
        output.playNote(['C3', 'D#3', 'G3'], 7);

         // Play a note at full velocity on all channels)
        output.playNote('F#-1', 'all', {velocity: 1});

        // Play a note on channel 16 in 2 seconds (relative time)
        output.playNote('F5', 16, {time: '+2000'});

        // Play a note on channel 1 at an absolute time in the future
        output.playNote('F5', 16, {time: WebMidi.time + 3000});

        // Play a note for a duration of 2 seconds (will send a note off message in 2 seconds). Also use
        // a low attack velocity
        output.playNote('Gb2', 10, {duration: 2000, velocity: 0.25});

        // Stop a playing note on all channels
        output.stopNote('C-1');

        // Stopping a playing note on channel 11
        output.stopNote('F3', 11);

        // Stop a playing note on channel 11 and use a high release velocity
        output.stopNote('G8', 11, {velocity: 0.9});

        // Stopping a playing note in 2.5 seconds
        output.stopNote('Bb2', 11, {time: '+2500'});

        // Send polyphonic aftertouch message to channel 8
        output.sendKeyAftertouch('C#3', 8, 0.25);

        // Send pitch bend (between -1 and 1) to channel 12
        output.sendPitchBend(-1, 12);

        // You can chain most method calls
        output.playNote('G5', 12)
          .sendPitchBend(-0.5, 12, {time: 400}) // After 400 ms.
          .sendPitchBend(0.5, 12, {time: 800})  // After 800 ms.
          .stopNote('G5', 12, {time: 1200});    // After 1.2 s.

          console.log('playing[2]');
      }
    });
  }

  start() {
    console.log('start');
    let nav: any = navigator;
    if (nav.requestMIDIAccess) {
      nav.requestMIDIAccess({
        sysex: false // this defaults to 'false' and we won't be covering sysex in this article.
      }).then(this.onMIDISuccess.bind(this), this.onMIDIFailure);
    } else {
      console.error('No MIDI support in your browser.');
    }
  }

  private onMIDISuccess(midiAccess) {
    // when we get a succesful response, run this code
    console.log('MIDI Access Object', midiAccess);
    // var inputs = midiAccess.inputs.values();
    // console.log('inputs: ');
    // console.log(inputs);
    // for (var input = inputs.next(); input && !input.done; input = inputs.next()) {
    //     // each time there is a midi message call the onMIDIMessage function
    //     console.log('--input: %o', input);
    //     input.value.onmidimessage = this.onMIDIMessage;
    // }
    var outputs = midiAccess.outputs.values();
    console.log('outputs: ');
    console.log(outputs);
    for (var output = outputs.next(); output && !output.done; output = outputs.next()) {
        // each time there is a midi message call the onMIDIMessage function
        console.log('--output: %o', output);
        output.value.onmidimessage = this.onMIDIMessage;
        /*
        connection: 'closed'
id:'6FF5590044F4859ED50C5167BCFE9700A1798E39AA55A628E86D39011FAECD5D'
manufacturer:''
name:'Midi Through Port-0'
onstatechange:null
state:'connected'
type:'output'
version:'ALSA library version 1.0.25'
        */
    }
  }

  private onMIDIFailure(e) {
    // when we get a failed response, run this code
    console.log('No access to MIDI devices or your browser doesn\'t support WebMIDI API. Please use WebMIDIAPIShim ' + e);
  }

  private onMIDIMessage(message) {
    console.log('midi message: %o', message);
  }
}


// function

